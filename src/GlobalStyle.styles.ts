import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body {
    margin: 0;
    background: #FDFCFA;
    font-family: sans-serif;
  }
`;
