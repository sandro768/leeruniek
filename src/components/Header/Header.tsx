import React from 'react';
import { AppBar, Box, Toolbar, Typography } from '@mui/material';

const Header = () => (
  <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static">
      <Toolbar>
        <Typography
          variant="h6"
          noWrap
          component="div"
          sx={{ display: { xs: 'none', sm: 'block' } }}
        >
          Leeruniek
        </Typography>
      </Toolbar>
    </AppBar>
  </Box>
);

export default Header;
