import React from 'react';
import GlobalStyles from './GlobalStyle.styles';
import HomePage from './pages/HomePage/HomePage';

const App = () => (
  <>
    <GlobalStyles />
    <HomePage />
  </>
);

export default App;
