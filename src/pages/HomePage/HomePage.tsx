import React from 'react';
import { Box, Typography } from '@mui/material';
import Header from '../../components/Header/Header';
import { CATEGORIES, NOTES, PLAN } from '../../data';
import { Category, Note, Plan } from './HomePage.styles';

const HomePage = () => {
  const unarchivedSortedCategories = CATEGORIES.filter(
    (category) => category.groupPlanId === PLAN.id && !category.isArchived
  ).sort((a, b) => a.name.localeCompare(b.name));

  const getNotes = (categoryId: number) =>
    NOTES.sort((a, b) => {
      const aDate = new Date(a.dateCreated);
      const bDate = new Date(b.dateCreated);

      return aDate.getTime() - bDate.getTime();
    }).map((note) =>
      note.groupPlanId === PLAN.id && note.categoryId === categoryId ? (
        <Box key={note.id} py={1}>
          <Note>{note.content}</Note>
        </Box>
      ) : null
    );

  return (
    <>
      <Header />
      <Box px={8}>
        <Typography variant="h5" my={1}>
          Plans
        </Typography>
        <Plan>
          <Typography variant="body1">{PLAN.name}</Typography>
          <Typography variant="body1" color="rgba(0, 0, 0, 0.6)">
            {PLAN.userCreated}
          </Typography>
        </Plan>
        <Typography variant="h5" my={1}>
          Categories
        </Typography>
        {unarchivedSortedCategories
          .filter((category) => category.parentNoteCategoryId === null)
          .map((category) => (
            <Category key={category.id}>
              <Typography variant="body1">{category.name}</Typography>
              {unarchivedSortedCategories
                .filter((cat) => cat.parentNoteCategoryId === category.id)
                .map((subCategory) => (
                  <Category key={subCategory.id + subCategory.name}>
                    <Typography variant="body2">{subCategory.name}</Typography>
                    {getNotes(subCategory.id)}
                  </Category>
                ))}
              {getNotes(category.id)}
            </Category>
          ))}
      </Box>
    </>
  );
};

export default HomePage;
