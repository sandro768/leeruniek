import styled from 'styled-components';

export const Plan = styled.div`
  display: inline-block;
  border-radius: 5px;
  border: 1px solid;
  padding: 20px;
  background-color: rgba(25, 118, 210, 0.1);
`;

export const Category = styled.div`
  padding: 8px 16px;
`;

export const Note = styled.div`
  position: relative;
  border-radius: 4px;
  padding: 16px 32px;
  max-width: 100px;
  background-color: rgb(252, 254, 168);
`;
